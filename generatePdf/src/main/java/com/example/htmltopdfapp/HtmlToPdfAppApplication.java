package com.example.htmltopdfapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HtmlToPdfAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(HtmlToPdfAppApplication.class, args);
    }

}
