package com.example.htmltopdfapp;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.html.simpleparser.HTMLWorker;
import com.lowagie.text.pdf.PdfDocument;
import com.lowagie.text.pdf.PdfWriter;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;
import org.xhtmlrenderer.pdf.ITextRenderer;

import java.io.*;
@Service
public class ClassLoader {


    public String parseThymeleafTemplate() {
        ClassLoaderTemplateResolver templateResolver = new ClassLoaderTemplateResolver();
        templateResolver.setSuffix(".html"); // specified which templates can be resolved
        templateResolver.setTemplateMode(TemplateMode.HTML);//Sets the template mode to be applied to templates resolved

        TemplateEngine templateEngine = new TemplateEngine();// This method receives a template name
        templateEngine.setTemplateResolver((ITemplateResolver) templateResolver);

        Context context = new Context();
        context.setVariable("to", "1 MIN script");

        return templateEngine.process("thymeleaf_template", context);//Process a template starting from a template (usually the template name).
        // Output will be written into a String that will be returned from calling this method,
    }

    public void generatePdfFromHtml(String html) throws IOException, DocumentException {
       String outputFolder = "thymeleaf.pdf";
        OutputStream outputStream = new FileOutputStream(outputFolder);

        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(html);
        renderer.layout();
        renderer.createPDF(outputStream);

        outputStream.close();

    }

}
