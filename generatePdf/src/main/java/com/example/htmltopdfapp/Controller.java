package com.example.htmltopdfapp;

import com.lowagie.text.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@RestController
@RequestMapping("/classLoader")
public class Controller {
    @Autowired
    ClassLoader classLoader;


    @GetMapping("/generatepdf")
    public void generatePdfFromHtml() throws IOException, DocumentException {
        String path = "C:\\Users\\onemi\\Desktop\\generator\\resume-generator\\facture\\facture29.html" ;
        String content = Files.readString(Paths.get(path));
        classLoader.generatePdfFromHtml(content);
    }
}
