package com.example.htmltopdfapp;

import com.lowagie.text.DocumentException;

import java.io.IOException;

public class Main {
    public static void main(String[] args) throws DocumentException, IOException {
        ClassLoader classLoader=new ClassLoader();
        String res=classLoader.parseThymeleafTemplate();
        classLoader.generatePdfFromHtml(res);
    }
}
